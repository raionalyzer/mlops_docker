from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="Tests with docker in mlops projects",
    author="rationalyzer",
    license="MIT",
)
