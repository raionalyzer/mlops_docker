FROM mambaorg/micromamba

WORKDIR /mlops_docker

COPY ./ ./

RUN micromamba create -n docker_env python=3.11 jupyter -c conda-forge -y

ENTRYPOINT [ "micromamba", "run", "-n", "docker_env", "jupyter", "notebook", "--ip=0.0.0.0", "--port=8899", "--allow-root"]
